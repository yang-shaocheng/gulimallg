package com.yang.gulimall.thirdparty.controller;

import com.yang.common.utils.R;
import com.yang.gulimall.thirdparty.component.SmsComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * ClassName: smsSendController
 * Package: com.yang.gulimall.thirdparty.controller
 * description:
 */
@RestController
@RequestMapping("/sms")
public class smsSendController {
    @Autowired
    private  SmsComponent smsComponent;
    //不是页面直接调用它，而是别的服务调用它，它只负责发验证码
    @GetMapping("/sendcode")
    public R sendCode(@RequestParam("phone") String phone,@RequestParam("code") String code){
        smsComponent.sendSmsCode(phone,code);
        return R.ok();
    }
}
