package com.yang.gulimall.seckill.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.yang.common.to.mq.SeckillOrderTo;
import com.yang.common.utils.R;
import com.yang.common.vo.MemberResponseVo;
import com.yang.gulimall.seckill.feign.CouponFeignService;
import com.yang.gulimall.seckill.feign.ProductFeignService;
import com.yang.gulimall.seckill.interceptor.LoginUserInterceptor;
import com.yang.gulimall.seckill.service.SeckillService;
import com.yang.gulimall.seckill.to.SeckillSkuRedisTo;
import com.yang.gulimall.seckill.vo.SeckillSessionWithSkusVo;
import com.yang.gulimall.seckill.vo.SkuInfoVo;
import org.redisson.api.RSemaphore;
import org.redisson.api.RedissonClient;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * ClassName: SeckillServiceImpl
 * Package: com.yang.gulimall.seckill.service.impl
 * description:
 */
@Service
public class SeckillServiceImpl implements SeckillService {
    @Autowired
    CouponFeignService couponFeignService;
    @Autowired
    StringRedisTemplate redisTemplate;
    private final  String SESSIONS_CACHE_PREFIX = "seckill:sessions:";

    private final  String SKUKILL_CACHE_PREFIX = "seckill:skus:";
    @Autowired
    ProductFeignService productFeignService;
    @Autowired
    RedissonClient redissonClient;
    @Autowired
    RabbitTemplate rabbitTemplate;

    private final String SKU_STOCK_SEMAPHORE = "seckill:stock:"; //+随机码
    @Override
    public void uploadSeckiilSkuLatest3Days() {
        //1.去数据库扫描最近三天参与秒杀的活动

        R session = couponFeignService.getLast3DaySession();
        if (session.getCode() == 0){
            //上架商品
            List<SeckillSessionWithSkusVo> sessionData = session.getData(new TypeReference<List<SeckillSessionWithSkusVo>>() {
            });
            //缓存到redis
            //1.缓存活动信息  key = start-end time  val = list<skuId>
            saveSessionInfos(sessionData);
            //2. 缓存活动关联的商品信息 key skuid val skuentity
            saveSessionSkuInfos(sessionData);

        }
    }
    //返回当前时间可以参与的秒杀商品信息
    @Override
    public List<SeckillSkuRedisTo> getCurrentSeckillSkus() {
        //1.确定当前时间属于哪个秒杀场次
        long time = new Date().getTime();
        Set<String> keys = redisTemplate.keys(SESSIONS_CACHE_PREFIX + "*");
        for (String key : keys) {
            String replace = key.replace(SESSIONS_CACHE_PREFIX, "");
            String[] s = replace.split("_");
            Long start = Long.parseLong(s[0]);
            Long end = Long.parseLong(s[1]);
            if (time>=start && time<=end){
                //2、获取这个秒杀哦场次的所有商品信息
                List<String> range = redisTemplate.opsForList().range(key, -100, 100);//拿到所有的PromotionSessionId_skuId
                BoundHashOperations<String, String, String> hashOps = redisTemplate.boundHashOps(SKUKILL_CACHE_PREFIX);
                List<String> list = hashOps.multiGet(range);//每一个元素都是一个商品json串
                if (list!=null){
                    List<SeckillSkuRedisTo> collect = list.stream().map(item -> {
                        SeckillSkuRedisTo redis = JSON.parseObject((String) item, SeckillSkuRedisTo.class);
                        return redis;
                    }).collect(Collectors.toList());
                    return collect;
                }
                break;
            }
        }
        return null;
    }
    //获取某一个商品的秒杀信息
    @Override
    public SeckillSkuRedisTo getSkuSeckillInfo(Long skuId) {
        //1、找到所有需要参与秒杀的商品的key
        BoundHashOperations<String, String, String> hashOps = redisTemplate.boundHashOps(SKUKILL_CACHE_PREFIX);
        Set<String> keys = hashOps.keys();
        //在这些key里面看有没有当前sku
        if (keys!=null&&keys.size()>0){
            String regx = "\\d_"+skuId;
            for (String key : keys) {
                if (Pattern.matches(regx,key)){
                    String json = hashOps.get(key);
                    SeckillSkuRedisTo skuRedisTo = JSON.parseObject(json, SeckillSkuRedisTo.class);
                    //判断是否返回随机码
                    long current = new Date().getTime();
                    Long startTime = skuRedisTo.getStartTime();
                    if (current>=startTime&&current<=skuRedisTo.getEndTime()){

                    }else {
                        skuRedisTo.setRandomCode(null);
                    }
                    return skuRedisTo;
                }
            }
        }

        return null;
    }
    //独立流程秒杀商品
    //TODO 上架秒杀商品的时候，每一个数据都有过期时间应该
    //TODO 秒杀的后续流程，增加运费
    @Override
    public String kill(String killId, String key, Integer num) {
        MemberResponseVo memberResponseVo = LoginUserInterceptor.loginUser.get();
        //1、合法性校验,获取当前商品的详细信息， killID
        BoundHashOperations<String, String, String> hashOps = redisTemplate.boundHashOps(SKUKILL_CACHE_PREFIX);
        String json = hashOps.get(killId);
        if (StringUtils.isEmpty(json)){
            return null;
        }else {
            SeckillSkuRedisTo redis = JSON.parseObject(json, SeckillSkuRedisTo.class);
            //校验合法性
            Long startTime = redis.getStartTime();
            Long endTime = redis.getEndTime();
            long time = new Date().getTime();
            long ttl =  endTime - time;
            //1、校验时间的合法性
            if (time>=startTime&&time<=endTime){
                //2、校验随机码和商品ID
                String randomCode = redis.getRandomCode();
                String skuId = redis.getPromotionSessionId()+"_"+redis.getSkuId();
                if (randomCode.equals(key)&&killId.equals(skuId)){
                    //3、校验数量
                   if (num<=redis.getSeckillLimit()){
                    //4、这个认是否已经购买过了,幂等性，如果只要秒杀成功、就去redis占位 userId_SessionId_skuId
                     String redisKey =  memberResponseVo.getId()+"_"+skuId;
                     //自动过期
                       Boolean aBoolean = redisTemplate.opsForValue().setIfAbsent(redisKey, num.toString(), ttl, TimeUnit.MILLISECONDS);
                       if (aBoolean){
                           //占位成功说明从来没买过，可以获取信号量了
                           RSemaphore semaphore = redissonClient.getSemaphore(SKU_STOCK_SEMAPHORE + randomCode);

                               boolean b = semaphore.tryAcquire(num);
                               if (b){

                               //拿到信号量，秒杀成功，快速下单，发送MQ消息 10ms
                               String timeId = IdWorker.getTimeId();
                               SeckillOrderTo orderTo = new SeckillOrderTo();
                               orderTo.setOrderSn(timeId);
                               orderTo.setMemberId(memberResponseVo.getId());
                               orderTo.setNum(num);
                               orderTo.setPromotionSessionId(redis.getPromotionSessionId());
                               orderTo.setSkuId(redis.getSkuId());
                               orderTo.setSeckillPrice(redis.getSeckillPrice());
                               rabbitTemplate.convertAndSend("order-event-exchange","order.seckill.order",orderTo);
                               return timeId;
                               }
                               return null;


                       }else {
                           //说明已经买过了
                           return  null;
                       }
                   }

                }else {
                    return null;
                }
            }else {
                return  null;
            }
        }

        return null;
    }

    //1.缓存活动信息  key = start-end time  val = list<skuId>
    private  void  saveSessionInfos(List<SeckillSessionWithSkusVo> sessions){
        if (sessions!=null){
        sessions.stream().forEach(session->{
            Long startTime = session.getStartTime().getTime();
            Long endTime = session.getEndTime().getTime();
            String key = SESSIONS_CACHE_PREFIX + startTime + "_" + endTime;
            Boolean hasKey = redisTemplate.hasKey(key);
            if (!hasKey){
            List<String> collect = session.getRelationSkus().stream().map(item ->item.getPromotionSessionId().toString()+"_"+item.getSkuId().toString()).collect(Collectors.toList());
            redisTemplate.opsForList().leftPushAll(key,collect);
            }
        });
        }
    }
    //2. 缓存活动关联的商品信息 保存hash  key  key skuid val SeckillSkuVo的json
    private  void  saveSessionSkuInfos(List<SeckillSessionWithSkusVo> sessions){
        sessions.stream().forEach(session->{
            //准备hash操作
            BoundHashOperations<String, Object, Object> ops = redisTemplate.boundHashOps(SKUKILL_CACHE_PREFIX);
            session.getRelationSkus().stream().forEach(seckillSkuVo -> {
                //4、随机码 防止脚本攻击
                String token = UUID.randomUUID().toString().replace("_", "");
                if (!ops.hasKey(seckillSkuVo.getPromotionSessionId().toString()+"_"+seckillSkuVo.getSkuId().toString())){
                //缓存商品
                SeckillSkuRedisTo redisTo = new SeckillSkuRedisTo();
                //1、sku的基本数据
                R info = productFeignService.getSkuInfo(seckillSkuVo.getSkuId());
                if (info.getCode() == 0){
                SkuInfoVo skuInfo = info.getData("skuInfo", new TypeReference<SkuInfoVo>() {
                });
                redisTo.setSkuInfo(skuInfo);
                }
                //2、sku的秒杀信息
                BeanUtils.copyProperties(seckillSkuVo,redisTo);
                //3、当前商品的秒杀时间信息
                redisTo.setStartTime(session.getStartTime().getTime());
                redisTo.setEndTime(session.getEndTime().getTime());

                redisTo.setRandomCode(token);
                String jsonString = JSON.toJSONString(redisTo);
                ops.put(seckillSkuVo.getPromotionSessionId().toString()+"_"+seckillSkuVo.getSkuId().toString(),jsonString);
                //如果当前这个场次的商品的库存信息已经上架就不需要上架了
                //5、使用库存作为分布式的信号量,限流
                    RSemaphore semaphore = redissonClient.getSemaphore(SKU_STOCK_SEMAPHORE + token);
                    //商品可以秒杀的数量作为信号量
                    semaphore.trySetPermits(seckillSkuVo.getSeckillCount());
                }
            });

        });


    }
}
