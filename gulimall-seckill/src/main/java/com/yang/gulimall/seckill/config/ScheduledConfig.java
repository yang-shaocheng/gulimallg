package com.yang.gulimall.seckill.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * ClassName: ScheduledConfig
 * Package: com.yang.gulimall.seckill.config
 * description:
 */
@EnableAsync
@Configuration
@EnableScheduling
public class ScheduledConfig {
}
