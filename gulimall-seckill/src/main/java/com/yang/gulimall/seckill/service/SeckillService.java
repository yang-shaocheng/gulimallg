package com.yang.gulimall.seckill.service;

import com.yang.gulimall.seckill.to.SeckillSkuRedisTo;

import java.util.List;

/**
 * ClassName: SeckillService
 * Package: com.yang.gulimall.seckill.service
 * description:
 */
public interface SeckillService {
    void uploadSeckiilSkuLatest3Days();

    List<SeckillSkuRedisTo> getCurrentSeckillSkus();

    SeckillSkuRedisTo getSkuSeckillInfo(Long skuId);

    String kill(String killId, String key, Integer num);
}
