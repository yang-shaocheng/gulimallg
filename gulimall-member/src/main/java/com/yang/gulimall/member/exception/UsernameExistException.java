package com.yang.gulimall.member.exception;

/**
 * ClassName: UsernameExistException
 * Package: com.yang.gulimall.member.exception
 * description:
 */
public class UsernameExistException extends  RuntimeException{
    public UsernameExistException() {
        super("用户名存在");
    }
}
