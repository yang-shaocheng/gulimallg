package com.yang.gulimall.member.exception;

/**
 * ClassName: PhoneExistException
 * Package: com.yang.gulimall.member.exception
 * description:
 */
public class PhoneExistException extends RuntimeException{
    public PhoneExistException() {
        super("手机好存在");
    }
}
