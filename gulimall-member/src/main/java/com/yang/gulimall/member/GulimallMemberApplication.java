package com.yang.gulimall.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.session.config.annotation.web.http.EnableSpringHttpSession;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

// 1.导入openfeign依赖2.编写一个openfeign接口，里面的声明的方法和远程需要调用的方法 mapping 方法体保持一致 3.启动类enable openfeign功能并指定包位置
@EnableFeignClients(basePackages = "com/yang/gulimall/member/feign")
@SpringBootApplication
@EnableDiscoveryClient
@EnableRedisHttpSession
public class GulimallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallMemberApplication.class, args);
    }

}
