package com.yang.gulimall.member.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

/**
 * ClassName: MemberRegistVo
 * Package: com.yang.gulimall.member.vo
 * description:
 */
@Data
public class MemberRegistVo {

    private String userName;


    private String password;


    private String phone;
}
