package com.yang.gulimall.member.config;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * ClassName: GuliFeignConfig
 * Package: com.yang.gulimall.order.config
 * description:
 */
@Configuration
public class GuliFeignConfig {
    @Bean("requestInterceptor")
    public RequestInterceptor requestInterceptor(){
        return new RequestInterceptor() {
            @Override
            public void apply(RequestTemplate requestTemplate) {
                //1.拿到刚进来的请求，从请求里面获取最初的请求头数据
                ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
                if (requestAttributes!=null){
                HttpServletRequest request = requestAttributes.getRequest();//老请求
               if (request!=null){
                //2,同步请求头数据 cookie
                String cookie = request.getHeader("Cookie");
                requestTemplate.header("Cookie",cookie); //新请求添加cookie
               }
                }
            }
        };
    }
}
