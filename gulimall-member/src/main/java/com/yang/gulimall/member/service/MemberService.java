package com.yang.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yang.common.utils.PageUtils;
import com.yang.gulimall.member.entity.MemberEntity;
import com.yang.gulimall.member.exception.PhoneExistException;
import com.yang.gulimall.member.exception.UsernameExistException;
import com.yang.gulimall.member.vo.MemberLoginVo;
import com.yang.gulimall.member.vo.MemberRegistVo;

import java.util.Map;

/**
 * 会员
 *
 * @author yangshaocheng
 * @email 515085554@qq.com
 * @date 2024-01-25 16:04:33
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void regist(MemberRegistVo vo);

    void checkPhoneUnique(String phone) throws PhoneExistException;

    void checkUsernameUnique(String username) throws UsernameExistException;

    MemberEntity login(MemberLoginVo vo);
}

