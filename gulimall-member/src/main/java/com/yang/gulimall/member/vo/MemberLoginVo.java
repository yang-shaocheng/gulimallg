package com.yang.gulimall.member.vo;

import lombok.Data;

/**
 * ClassName: MemberLoginVo
 * Package: com.yang.gulimall.member.vo
 * description:
 */
@Data
public class MemberLoginVo {
    private  String loginacct;
    private  String password;
}
