package com.yang.common.to;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * ClassName: SkuReductionTo
 * Package: com.yang.common.to
 * description:
 */
@Data
public class SkuReductionTo {
    private Long  skuId;
    private int fullCount;
    private BigDecimal discount;
    private int countStatus;
    private BigDecimal fullPrice;
    private BigDecimal reducePrice;
    private int priceStatus;
    private List<MemberPrice> memberPrice;
}
