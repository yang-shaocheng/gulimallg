package com.yang.common.to.mq;

import lombok.Data;

import java.util.List;

/**
 * ClassName: StockLockedTo
 * Package: com.yang.common.to.mq
 * description:
 */
@Data
public class StockLockedTo {
    private Long id; //库存工作单id
    private StockDetailTo detailTo;//详情单的所有id
}
