package com.yang.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * ClassName: SpuBoundTo
 * Package: com.yang.common.to
 * description:
 */
@Data
public class SpuBoundTo {
    private  Long spuId;
    private BigDecimal buyBounds;
    private BigDecimal growBounds;

}
