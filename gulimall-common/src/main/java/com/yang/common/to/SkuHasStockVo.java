package com.yang.common.to;

import lombok.Data;

/**
 * ClassName: SkuHasStockVo
 * Package: com.yang.gulimall.ware.vo
 * description:
 */
@Data
public class SkuHasStockVo {
    private  Long skuId;
    private  Boolean hasStock;


}
