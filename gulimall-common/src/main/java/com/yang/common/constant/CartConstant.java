package com.yang.common.constant;

/**
 * ClassName: CartConstant
 * Package: com.yang.common.constant
 * description:
 */
public class CartConstant {
    public  static   final String TEMP_USER_COOKIE_NAME = "user-key";
    public  static   final int TEMP_USER_COOKIE_TIMEOUT = 60*60*24*30;
}
