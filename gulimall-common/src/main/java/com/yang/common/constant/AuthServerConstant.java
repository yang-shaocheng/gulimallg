package com.yang.common.constant;

/**
 * ClassName: AuthServerConstant
 * Package: com.yang.common.constant
 * description:
 */
public class AuthServerConstant {
    public static final String SMS_CODE_CACHE_PREFIX="sms:code:";
    public static final String LOGIN_USER="loginUser";
}
