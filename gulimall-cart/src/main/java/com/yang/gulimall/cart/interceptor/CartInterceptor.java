package com.yang.gulimall.cart.interceptor;

import com.yang.common.constant.AuthServerConstant;
import com.yang.common.constant.CartConstant;
import com.yang.common.vo.MemberResponseVo;
import com.yang.gulimall.cart.vo.UserInfoTo;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.UUID;

/**
 * ClassName: CartInterceptor
 * Package: com.yang.gulimall.cart.interceptor
 * description:
 */

public class CartInterceptor implements HandlerInterceptor {
    public   static  ThreadLocal<UserInfoTo> threadLocal = new ThreadLocal<>();
    //目标方法执行之前拦截，判断用户登录状态，并封装给controller目标请求
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        UserInfoTo userInfoTo = new UserInfoTo();
        HttpSession session = request.getSession();
        MemberResponseVo member = (MemberResponseVo) session.getAttribute(AuthServerConstant.LOGIN_USER);
        if (member != null){
            //已登录
            userInfoTo.setUserId(member.getId());
        }
        //未登录
        Cookie[] cookies = request.getCookies();
        if (cookies !=null&&cookies.length>0){
            for (Cookie cookie : cookies) {
                //匹配 user-key
                String name = cookie.getName();
                if (name.equals(CartConstant.TEMP_USER_COOKIE_NAME)){
                    userInfoTo.setUserKey(cookie.getValue());
                    userInfoTo.setTempUser(true);
                }
            }
        }
        //既没有登录又没有临时用户，需要创建一个UUID作为临时用户的user-key
        if (StringUtils.isEmpty(userInfoTo.getUserKey())){
            String uuid = UUID.randomUUID().toString();
            userInfoTo.setUserKey(uuid);
        }

        //目标方法执行之前放置数据
        threadLocal.set(userInfoTo);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        UserInfoTo userInfoTo = threadLocal.get();
        if (!userInfoTo.isTempUser()){
        Cookie cookie = new Cookie(CartConstant.TEMP_USER_COOKIE_NAME, userInfoTo.getUserKey());
        cookie.setDomain("gulimall.com");
        cookie.setMaxAge(CartConstant.TEMP_USER_COOKIE_TIMEOUT);
        response.addCookie(cookie);
        }
    }
}
