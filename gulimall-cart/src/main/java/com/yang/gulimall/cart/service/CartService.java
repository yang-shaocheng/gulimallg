package com.yang.gulimall.cart.service;

import com.yang.gulimall.cart.vo.CartItemVo;
import com.yang.gulimall.cart.vo.CartVo;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * ClassName: CartService
 * Package: com.yang.gulimall.cart.service
 * description:
 */
public interface CartService {
    CartItemVo addToCart(Long skuId, Integer num) throws ExecutionException, InterruptedException;

    CartItemVo getCartItemVo(Long skuId);

    CartVo getcart() throws ExecutionException, InterruptedException;

    void clearCart(String cartKey);

    void checkItem(Long skuId, Integer check);

    void changeItemCount(Long skuId, Integer num);

    void deleteItem(Long skuId);

    List<CartItemVo> getUserCartItems();
}
