package com.yang.gulimall.cart.controller;

import com.yang.common.constant.AuthServerConstant;
import com.yang.gulimall.cart.interceptor.CartInterceptor;
import com.yang.gulimall.cart.service.CartService;
import com.yang.gulimall.cart.vo.CartItemVo;
import com.yang.gulimall.cart.vo.CartVo;
import com.yang.gulimall.cart.vo.UserInfoTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * ClassName: CartController
 * Package: com.yang.gulimall.cart.controller
 * description:1.浏览器有一个cookie，表示访问的用户，一个月后过期
 *              2.如果第一次使用购物车功能，会给一个临时用户
 *              3.浏览器保存，以后每次访问都会带上
 *              登录状态：session有
 *              没登录状态：按照cookie里的带的user-key来做
 *              第一次什么都没有，给一个临时用户
 */
@Controller
public class CartController {
    @Autowired
    private CartService cartService;
    @GetMapping("/currentUserCartItems")
    @ResponseBody
    public List<CartItemVo> getCurrentUserCartItems(){
        return cartService.getUserCartItems();
    }
    @GetMapping("/deleteItem")
    public String deleteItem(@RequestParam("skuId") Long skuId){
        cartService.deleteItem(skuId);
        return  "redirect:http://cart.gulimall.com/cart.html";
    }

    @GetMapping("/countItem")
    public String countItem(@RequestParam("skuId") Long skuId,@RequestParam("num") Integer num){
        cartService.changeItemCount(skuId,num);
        return  "redirect:http://cart.gulimall.com/cart.html";
    }

    @GetMapping("/checkItem")
    public String checkItem(@RequestParam("skuId") Long skuId,@RequestParam("checked") Integer check){
        cartService.checkItem(skuId,check);
        return  "redirect:http://cart.gulimall.com/cart.html";
    }

    @GetMapping("/cart.html")
    public  String cartListPage(Model model ) throws ExecutionException, InterruptedException {
//        //1.快速得到用户信息
//        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        CartVo cart = cartService.getcart();
        model.addAttribute("cart",cart);
        return "cartList";
    }
    @GetMapping("/addToCart")
    public  String addToCart(@RequestParam("skuId") Long skuId, @RequestParam("num") Integer num, RedirectAttributes ra) throws ExecutionException, InterruptedException {
        cartService.addToCart(skuId,num);
        ra.addAttribute("skuId",skuId);
        return  "redirect:http://cart.gulimall.com/addToCartSuccess.html";
    }
    //商品成功添加后为避免重复刷单，重定向到一个新的页面
    @GetMapping("/addToCartSuccess.html")
    public String addToCartSuccessPage(@RequestParam("skuId") Long skuId,Model model){
        //重定向到成功页面，为了可以重复刷新，再次查询购物车数据
        CartItemVo itemvo = cartService.getCartItemVo(skuId);
        model.addAttribute("item",itemvo);
        return "success";
    }
}
