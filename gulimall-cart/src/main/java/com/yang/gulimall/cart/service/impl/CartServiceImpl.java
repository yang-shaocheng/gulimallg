package com.yang.gulimall.cart.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.yang.common.utils.R;
import com.yang.gulimall.cart.feign.ProductFeignService;
import com.yang.gulimall.cart.interceptor.CartInterceptor;
import com.yang.gulimall.cart.service.CartService;
import com.yang.gulimall.cart.vo.CartItemVo;
import com.yang.gulimall.cart.vo.CartVo;
import com.yang.gulimall.cart.vo.SkuInfoVo;
import com.yang.gulimall.cart.vo.UserInfoTo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

/**
 * ClassName: CartServiceImpl
 * Package: com.yang.gulimall.cart.service.impl
 * description:
 */
@Slf4j
@Service
public class CartServiceImpl implements CartService {
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private ProductFeignService productFeignService;
    @Autowired
    private ThreadPoolExecutor executor;

    private final  String CART_PREFIX = "gulimall:cart:";

    @Override
    public CartItemVo addToCart(Long skuId, Integer num) throws ExecutionException, InterruptedException {
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        String res = (String) cartOps.get(skuId.toString());
        if (StringUtils.isEmpty(res)){
        //1.添加新商品到购物车 新建一个购物车项用于添加
        CartItemVo cartItemVo = new CartItemVo();
        //2.远程查询要添加的商品信息
        CompletableFuture<Void> getSkuInfoTask = CompletableFuture.runAsync(() -> {
            R skuInfo = productFeignService.getSkuInfo(skuId);
            SkuInfoVo data = skuInfo.getData("skuInfo", new TypeReference<SkuInfoVo>() {
            });
            cartItemVo.setCheck(true);
            cartItemVo.setCount(num);
            cartItemVo.setImage(data.getSkuDefaultImg());
            cartItemVo.setPrice(data.getPrice());
            cartItemVo.setTitle(data.getSkuTitle());
            cartItemVo.setSkuId(skuId);
        }, executor);
        //3.远程查询SKU组合信息
        CompletableFuture<Void> getSkuSaleAttrValues = CompletableFuture.runAsync(() -> {
            List<String> values = productFeignService.getSkuSaleAttrValues(skuId);
            cartItemVo.setSkuAttrValues(values);
        }, executor);
        CompletableFuture.allOf(getSkuInfoTask,getSkuSaleAttrValues).get();
        String s = JSON.toJSONString(cartItemVo);
        cartOps.put(skuId.toString(),s);
        return cartItemVo;
        }else {
            //购物车有此商品，只修改数量
            CartItemVo cartItemVo = JSON.parseObject(res, CartItemVo.class);
            cartItemVo.setCount(cartItemVo.getCount()+num);
            cartOps.put(skuId.toString(),JSON.toJSONString(cartItemVo));
            return  cartItemVo;
        }
    }

    @Override
    public CartItemVo getCartItemVo(Long skuId) {
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        String str = (String) cartOps.get(skuId.toString());
        CartItemVo cartItemVo = JSON.parseObject(str, CartItemVo.class);
        return cartItemVo;
    }

    @Override
    public CartVo getcart() throws ExecutionException, InterruptedException {
        CartVo cart = new CartVo();
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        if (userInfoTo.getUserId() != null){
            //1.登录
            String  cartKey = CART_PREFIX+userInfoTo.getUserId();
            BoundHashOperations<String, Object, Object> hashOps = redisTemplate.boundHashOps(cartKey);
            //2.如果临时购物车的数据没有合并
            String tempCartKey = CART_PREFIX + userInfoTo.getUserKey();
            List<CartItemVo> cartItems = getCartItems(tempCartKey);
            if (cartItems!=null){
                //临时购物车有数据，需要合并
            for (CartItemVo cartItem : cartItems) {
                addToCart(cartItem.getSkuId(),cartItem.getCount());
            }
            //清空临时购物车的数据
                clearCart(tempCartKey);
            }
            //3.获取合并后的包含用户自己购物车和临时购物车的数据
            List<CartItemVo> items = getCartItems(cartKey);
            cart.setItems(items);
        }else {
            //没登录
            String cartKey = CART_PREFIX+userInfoTo.getUserKey();
            //获取临时购物车的所有购物项
            List<CartItemVo> cartItems = getCartItems(cartKey);
            cart.setItems(cartItems);
        }
        return cart;
    }



    //获取要操作的购物车
    public  BoundHashOperations<String, Object, Object>  getCartOps(){
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        String cartKey = "";
        if (userInfoTo.getUserId() != null){
            cartKey = CART_PREFIX+userInfoTo.getUserId();
        }else {
            cartKey = CART_PREFIX+userInfoTo.getUserKey();
        }
        BoundHashOperations<String, Object, Object> operations = redisTemplate.boundHashOps(cartKey);
        return  operations;
    }

    //获取购物车项
    private List<CartItemVo> getCartItems(String cartKey){
        BoundHashOperations<String, Object, Object> hashOps = redisTemplate.boundHashOps(cartKey);
        List<Object> values = hashOps.values();
        if (values!=null&&values.size()>0){
            List<CartItemVo> collect = values.stream().map((obj) -> {
                String str = String.valueOf(obj);
                CartItemVo cartItemVo = JSON.parseObject(str, CartItemVo.class);
                return cartItemVo;
            }).collect(Collectors.toList());
            return collect;
        }
        return  null;
    }
    //清空购物车
    @Override
    public void clearCart(String cartKey) {
        redisTemplate.delete(cartKey);

    }
    //勾选购物项
    @Override
    public void checkItem(Long skuId, Integer check) {
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        CartItemVo cartItemVo = getCartItemVo(skuId);
        cartItemVo.setCheck(check==1?true:false);
        String s = JSON.toJSONString(cartItemVo);
        cartOps.put(skuId.toString(),s);
    }
    //修改购物项数量
    @Override
    public void changeItemCount(Long skuId, Integer num) {
        CartItemVo cartItemVo = getCartItemVo(skuId);
        cartItemVo.setCount(num);
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        String s = JSON.toJSONString(cartItemVo);
        cartOps.put(skuId.toString(),s);

    }
    //删除购物项
    @Override
    public void deleteItem(Long skuId) {
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        cartOps.delete(skuId.toString());
    }

    @Override
    public List<CartItemVo> getUserCartItems() {
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        if (userInfoTo.getUserId() == null){
            return  null;
        }else {
            String cartKey = CART_PREFIX+userInfoTo.getUserId();
            List<CartItemVo> cartItems = getCartItems(cartKey);
            //过滤，获取所有被选中的购物车项
            List<CartItemVo> collect = cartItems.stream()
                    .filter(item -> item.getCheck())
                    .map(item->{
                        BigDecimal price = productFeignService.getPrice(item.getSkuId());
                        //更新为最新价格
                        item.setPrice(price);
                        return item;
                    })
                    .collect(Collectors.toList());
            return  collect;
        }

    }
}
