package com.yang.gulimall.cart.vo;

import lombok.Data;
import lombok.ToString;

/**
 * ClassName: UserInfoTo
 * Package: com.yang.gulimall.cart.vo
 * description:
 */
@ToString
@Data
public class UserInfoTo {
    private  Long userId;
    private  String userKey;
    private boolean tempUser = false;
}
