package com.yang.gulimall.order.config;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;

import com.yang.gulimall.order.vo.PayVo;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "alipay")
@Component
@Data
public class AlipayTemplate {

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public String app_id = "9021000136651647";

    // 商户私钥，您的PKCS8格式RSA2私钥
    public String merchant_private_key = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCjIndVSYlEHtbnVsr4ayOPDbmNenO56KiddJz6BMpGE89Lf3X0Z9Y1ZuXAA+TIi3GWetzool0AL9XhrcNmbhKRfnQRKP72g1DfW3Nr2hs/QaYFjXOx+OyqmTMa/oWxZ7eb8jqBwPNR0QrZ4mrnUR837hyXP2nhSaf6VquGc6mH3MkDHZUSeb0pO20yQ89Uzov2P4W73u2wdjyUCZ8qMLWZvWc7rtRwMrTB0EkdtewP9fDJL5WEljJ8KSEF4qlC01VJJeNr3YrxzjFxqHCvi4nRLz/LoBepx67ncMuTZaas56plKujeMvGxkMxqEFpDAfvcEGPLsA1xzIAfS3QQE+IdAgMBAAECggEAIm470XxbxEzd/qWK4TN7pc23w+x8UFiSfekG2J36MyArODwGoTbpNwwooYYw20LTDneP+GmMR2XRNnh+LTg+IwcVaCnG5jCA+SNMEQ9PgOJSMAnSyVVIXhlHeAMQJIlHb1qIjqoysKn2KFZpSbkNZbiTwDhaGHp7QZLZUN9IgKv6HjLnd0lmD8zTEi+JL8gScj7jDL5J8lUKqNiaKaZwLczhWDUWnlta6KdPjl48NMopL1cAVkiXDOchtESCeLiPQNKmdd+NE0Sjmm+1zSroSKaz2uqH350o46dew/rsn9xWf4SpT0LtHTJItoaz+HBtRfIWNDWODiTslNDt0fwK4QKBgQDcbCsLVqH6KHxOIiSJ59J4VeNagxDSrYYwzFz8x5boHyhW/PqvUBSdPavGW85fXAComy37ELiUdFdbxPaxy7Ixkomk5u+qEZFa20d86LgosiKgnixt21aadVh5I97eRBKuk0nYFtZ07jAwOepnZ+3AvUDNKX00FCwHl6I8sauDOwKBgQC9dytKBr8VmX5damRanH1LQUd9ejlTtTk+msBFXDkVgb5jgouY/p5wR4tDIi+bv9VWg5U6QyLP03BfBw/4G93rbIs69MDQF7q2U7B1F82BCXNxGaQtGR6/cVmSofwJgWR8kBS5WWuoJKpiR9laqddFg80kgkuugdjVCWjoi64qhwKBgFQq1bDprsY4KWcocG1cYSjkGwGX5Jed/QyrLKK1j/Eo3J2C0jKZUJXOU8ZVjhMprXK0drVU6DZ/yk/XgR/ZGT0GqJqYOFHnjWp1KvIOZyvbAY/1dh+ozoQCZPs08EhNFBknUt45skBKJ4pywwgjTkz3t0PgINf/D+UCSMgGS+i7AoGAH7DZKs6qE6DTBCkRlB5szrOik5hoElpTsucLlg5w9en3wIlLdFHSipFM9/O15uVVd90WjuI/5mdFen+h6iwvGHvVZg0eWzA86NZiw5Mxqo28I4TYczqqa/c3tlEl6Skdd8BZ1w7f9QX9AedmscWK3nPHUn4AzrWlYSzrwUnr3e0CgYBtQQmakvt9NuMOiwUthA+jFzJcfgws38PKJfE3Aez+SPEhgXOaxn/+FXI1i6tEWqTcpoiXwS/XGe0UYV6zGvC5uGNciLzlvfTTJ6DcrmcJw4XPxxldwisGzbmXMQXCR37PDnKlhga0llFZRdRKfis8HE9s9mTELQKIjzIk0afj3w==";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAo4vVgZ9wRg9MN1uGbgBH2K2Rpsa3QawYqPonIfwiL6ntOgFTNxi/qqdtzojxWOWcd5SfVqOiBF+q7wcBG9YswI43A3FxbTY+GqLNg3Wekd2dEwz5An7gBOFrahnvabVAnBeZSUj8LWTcw1PPi4Aj/WOhS6qzvEwH8id77IDy7ZZkHxgZtbUXiQxRd/x2LPwFx/R2pQl2NmnQgWA0TvZu94Z60JLcQ+IokcGa/JI510c0C3x07Okzt96afLqAB2oVOJ2NRakgmA9dpwSwyt1cjdExifwscE6s+J3OJkcLfjiP1bgTOL/I+/dzmQrLSQ2semg343VYXMZn3X7z1XPfSQIDAQAB";

    // 服务器[异步通知]页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    // 支付宝会悄悄的给我们发送一个请求，告诉我们支付成功的信息
    public String notify_url = "http://3dtedd.natappfree.cc/payed/notify";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    //同步通知，支付成功，一般跳转到成功页
    public String return_url = "http://member.gulimall.com/memberOrder.html";

    // 签名方式
    private  String sign_type = "RSA2";

    // 字符编码格式
    private  String charset = "utf-8";

    //订单超时时间 ,比订单解锁时间提前1点稳妥点
    private String timeout = "1m";

    // 支付宝网关； https://openapi.alipaydev.com/gateway.do
    public String gatewayUrl = "https://openapi-sandbox.dl.alipaydev.com/gateway.do";

    public  String pay(PayVo vo) throws AlipayApiException {

        //AlipayClient alipayClient = new DefaultAlipayClient(AlipayTemplate.gatewayUrl, AlipayTemplate.app_id, AlipayTemplate.merchant_private_key, "json", AlipayTemplate.charset, AlipayTemplate.alipay_public_key, AlipayTemplate.sign_type);
        //1、根据支付宝的配置生成一个支付客户端
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl,
                app_id, merchant_private_key, "json",
                charset, alipay_public_key, sign_type);

        //2、创建一个支付请求 //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(return_url);
        alipayRequest.setNotifyUrl(notify_url);

        //商户订单号，商户网站订单系统中唯一订单号，必填
        String out_trade_no = vo.getOut_trade_no();
        //付款金额，必填
        String total_amount = vo.getTotal_amount();
        //订单名称，必填
        String subject = vo.getSubject();
        //商品描述，可空
        String body = vo.getBody();

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
                + "\"total_amount\":\""+ total_amount +"\","
                + "\"subject\":\""+ subject +"\","
                + "\"body\":\""+ body +"\","
                + "\"timeout_express\":\""+timeout+"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        String result = alipayClient.pageExecute(alipayRequest).getBody();

        //会收到支付宝的响应，响应的是一个页面，只要浏览器显示这个页面，就会自动来到支付宝的收银台页面
        System.out.println("支付宝的响应："+result);

        return result;

    }
}
