package com.yang.gulimall.order.web;

import com.yang.gulimall.order.service.OrderService;
import com.yang.gulimall.order.vo.OrderConfirmVo;
import com.yang.gulimall.order.vo.OrderSubmitVo;
import com.yang.gulimall.order.vo.SubmitOrderResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.math.BigDecimal;
import java.util.concurrent.ExecutionException;

/**
 * ClassName: OrderWebController
 * Package: com.yang.gulimall.order.web
 * description:
 */
@Controller
public class OrderWebController {
    @Autowired
    OrderService orderService;

    @GetMapping("/toTrade")
    public String toTrade(Model model) throws ExecutionException, InterruptedException {
        //展示订单确认的数据
        OrderConfirmVo confirmVo = orderService.confirmOrder();
        BigDecimal total = confirmVo.getTotal();
        model.addAttribute("orderConfirmData",confirmVo);
        return "confirm";
    }
    @PostMapping("/submitOrder")
    public  String submitOrder(OrderSubmitVo vo, Model model, RedirectAttributes redirectAttributes){
        SubmitOrderResponseVo responseVo = orderService.submitOrder(vo);
        if (responseVo.getCode() == 0){
        //下单成功，来到支付选择页
            model.addAttribute("submitOrderResp",responseVo);
            return "pay";
        }else {
        //下单失败，来到订单确认页
            String msg = "下单失败；";
            switch (responseVo.getCode()){
                case 1: msg+="订单信息过期，请刷新再次提交";break;
                case 2: msg+="订单商品价格发生变化，请确认后再次提交";break;
                case 3: msg+="库存锁定失败，商品库存不足";break;
            }
        redirectAttributes.addFlashAttribute("msg",msg);
        return  "redirect:http://order.gulimall.com/toTrade";
        }
    }
}
