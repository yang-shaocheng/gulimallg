package com.yang.gulimall.order.listener;

import com.rabbitmq.client.Channel;
import com.yang.gulimall.order.config.AlipayTemplate;
import com.yang.gulimall.order.entity.OrderEntity;
import com.yang.gulimall.order.service.OrderService;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * ClassName: OrderCloseListener
 * Package: com.yang.gulimall.order.listener
 * description:
 */
@Service
@RabbitListener(queues = "order.release.order.queue")
public class OrderCloseListener {
    @Autowired
    OrderService orderService;
    @Autowired
    AlipayTemplate alipayTemplate;
    @RabbitHandler
    public void listener(OrderEntity entity, Channel channel, Message message) throws IOException {
        System.out.println("收到过期的订单消息：准备关闭订单"+entity.getOrderSn());
        try {
            orderService.closeOrder(entity);
            //TODO 手动调用支付宝收单,让支付宝那边没法支付了

            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        }catch (Exception e){
            channel.basicReject(message.getMessageProperties().getDeliveryTag(),true);
        }

    }
}
