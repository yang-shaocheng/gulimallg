package com.yang.gulimall.order.web;

import com.alipay.api.AlipayApiException;
import com.yang.gulimall.order.config.AlipayTemplate;
import com.yang.gulimall.order.service.OrderService;
import com.yang.gulimall.order.vo.PayVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * ClassName: PayWebController
 * Package: com.yang.gulimall.order.web
 * description:
 */
@Controller
public class PayWebController {
    @Autowired
    AlipayTemplate alipayTemplate;
    @Autowired
    OrderService orderService;
    @ResponseBody
    @GetMapping(value = "/payOrder",produces = "text/html")
    public String payOrder(@RequestParam("orderSn") String orderSn) throws AlipayApiException {
//        PayVo payVo = new PayVo();
        PayVo payVo = orderService.getOrderPay(orderSn);//得到订单的备注、订单号、订单的主体、金额
        String pay = alipayTemplate.pay(payVo);
        System.out.println(pay);
        return pay;
    }
}
