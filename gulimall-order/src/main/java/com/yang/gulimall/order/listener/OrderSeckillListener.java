package com.yang.gulimall.order.listener;

import com.mysql.cj.log.Log;
import com.rabbitmq.client.Channel;
import com.yang.common.to.mq.SeckillOrderTo;
import com.yang.gulimall.order.entity.OrderEntity;
import com.yang.gulimall.order.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * ClassName: OrderSeckillListener
 * Package: com.yang.gulimall.order.listener
 * description:
 */
@Slf4j
@Component
@RabbitListener(queues = "order.seckill.order.queue")
public class OrderSeckillListener {
    @Autowired
    OrderService orderService;
    @RabbitHandler
    public void listener(SeckillOrderTo seckillOrderTo, Channel channel, Message message) throws IOException {

        try {
            log.info("准备创建秒杀单的详细信息");
            orderService.createSeckillOrder(seckillOrderTo);
            //TODO 手动调用支付宝收单,让支付宝那边没法支付了
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        }catch (Exception e){
            channel.basicReject(message.getMessageProperties().getDeliveryTag(),true);
        }

    }
}
