package com.yang.gulimall.order.vo;

import lombok.Data;

/**
 * ClassName: SkuHasStockVo
 * Package: com.yang.gulimall.ware.vo
 * description:
 */
@Data
public class SkuHasStockVo {
    private  Long skuId;
    private  boolean hasStock;
}
