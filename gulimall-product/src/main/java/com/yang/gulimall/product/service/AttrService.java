package com.yang.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yang.common.utils.PageUtils;
import com.yang.gulimall.product.entity.AttrEntity;
import com.yang.gulimall.product.entity.ProductAttrValueEntity;
import com.yang.gulimall.product.vo.AttrGroupRelationVo;
import com.yang.gulimall.product.vo.AttrRespVo;
import com.yang.gulimall.product.vo.AttrVo;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author yangshaocheng
 * @email 515085554@qq.com
 * @date 2024-01-24 15:50:37
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveAttr(AttrVo attr);

    PageUtils querybaseAttrPage(Map<String, Object> params, Long catelogId, String type);

    AttrRespVo getInfo(Long attrId);

    void updateAttr(AttrVo attr);

    List<AttrEntity> getRelationAttr(Long attrgroupId);

    void deleteVos(AttrGroupRelationVo[] aVos);

    PageUtils getNoRelationAttr(Map<String, Object> params, Long attrgroupId);

    //在指定的所有属性集合里面挑出检索属性
    List<Long> selectSearchAttrIds(List<Long> attrIds);
}

