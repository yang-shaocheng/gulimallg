package com.yang.gulimall.product.feign;

import com.yang.common.to.SkuReductionTo;
import com.yang.common.to.SpuBoundTo;
import com.yang.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * ClassName: SpuFeignService
 * Package: com.yang.gulimall.product.feign
 * description:
 */
@FeignClient("gulimall-coupon")
public interface CouponFeignService {
    /**
     *  1、CouponFeignService.saveSpuBounds(spuBoundTo);
     *     1.@RequestBody 将这个对象转为json.
     *     2.找到gulimall-coupon服务，给 /coupon/spubounds/save发送请求。
     *        将上一步转的json放在请求体位置，发送请求
     *     3.对方服务也就是gulimall-coupon收到请求，请求体里有json数据(@RequestBody SpuBoundsEntity spuBoundTo)。将请求体的json转为SpuBoundsEntity；
     *          也就是说只要json数据模型是兼容的，双方服务的请求体类型可以不一样，无需使用同一个to
     *
     */
    @PostMapping("/coupon/spubounds/save")
    R saveSpuBounds(@RequestBody SpuBoundTo spuBoundTo);
    @PostMapping("/coupon/skufullreduction/saveinfo")
    R saveSkuReduction(@RequestBody SkuReductionTo skuReductionTo);
}
