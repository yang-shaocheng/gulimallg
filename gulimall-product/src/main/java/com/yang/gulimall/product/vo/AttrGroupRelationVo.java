package com.yang.gulimall.product.vo;

import lombok.Data;

/**
 * ClassName: AttrGroupRelationVo
 * Package: com.yang.gulimall.product.vo
 * description:
 */
@Data
public class AttrGroupRelationVo {
    private Long attrId;
    private  Long attrGroupId;
}
