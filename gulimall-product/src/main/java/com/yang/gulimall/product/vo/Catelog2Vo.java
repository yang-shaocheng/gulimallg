package com.yang.gulimall.product.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * ClassName: CateLog2Vo
 * Package: com.yang.gulimall.product.vo
 * description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Catelog2Vo {
        private  String catelog1Id;  //1级父分类ID
        private List<Catalog3Vo> catalog3List; //3级子分类
        private  String id;
        private  String name;
        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        public  static  class  Catalog3Vo{ //3级分类内部类VO
            private  String catelog2Id;//2级父分类ID
            private  String id;
            private  String name;
        }

}
