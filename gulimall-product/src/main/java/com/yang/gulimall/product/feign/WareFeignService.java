package com.yang.gulimall.product.feign;

import com.yang.common.to.SkuHasStockVo;
import com.yang.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * ClassName: WareFeignService
 * Package: com.yang.gulimall.product.feign
 * description:
 */
@FeignClient("gulimall-ware")
public interface WareFeignService {
    //1.R设计的时候加上泛型，指明要返回的data数据到底是什么类型 2.不要返回R了，返回我们想要的结果 3.自己写个方法封装解析结果
    @PostMapping("/ware/waresku/hasstock")
    public  R getSkusHasStock(@RequestBody List<Long> skuIds);
}
