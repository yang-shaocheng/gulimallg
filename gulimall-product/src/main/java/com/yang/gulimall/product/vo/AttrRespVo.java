package com.yang.gulimall.product.vo;

import com.yang.gulimall.product.entity.AttrEntity;
import lombok.Data;

/**
 * ClassName: AttrRespVo
 * Package: com.yang.gulimall.product.vo
 * description:
 */
@Data
public class AttrRespVo extends AttrVo {
        private  String catelogName;
        private  String groupName;
         private  Long[] catelogPath;
}
