package com.yang.gulimall.product.vo;

import lombok.Data;

/**
 * ClassName: BrandVo
 * Package: com.yang.gulimall.product.vo
 * description:
 */
@Data
public class BrandVo {
    private  Long brandId;
    private  String brandName;
}

