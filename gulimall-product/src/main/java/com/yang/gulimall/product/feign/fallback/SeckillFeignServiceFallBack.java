package com.yang.gulimall.product.feign.fallback;

import com.yang.common.exception.BizCodeEnume;
import com.yang.common.utils.R;
import com.yang.gulimall.product.feign.SeckillFeignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * ClassName: SeckillFeignServiceFallBack
 * Package: com.yang.gulimall.product.feign.fallback
 * description:
 */
@Slf4j
@Component
public class SeckillFeignServiceFallBack implements SeckillFeignService {
    @Override
    public R getSkuSeckillInfo(Long skuId) {
        log.info("熔断方法调用。。。。");
        return R.error(BizCodeEnume.TOO_MANY_REQUEST.getCode(),BizCodeEnume.TOO_MANY_REQUEST.getMsg());
    }
}
