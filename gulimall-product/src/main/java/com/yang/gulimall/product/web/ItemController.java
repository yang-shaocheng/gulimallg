package com.yang.gulimall.product.web;

import com.yang.gulimall.product.service.SkuInfoService;
import com.yang.gulimall.product.vo.SkuItemVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.concurrent.ExecutionException;

/**
 * ClassName: ItemController
 * Package: com.yang.gulimall.product.web
 * description:
 */
@Controller
public class ItemController {
    @Autowired
    private SkuInfoService skuInfoService;
    /**
     *
     * @展示sku详情
     */
    @GetMapping("/{skuId}.html")
    public String skuItem(@PathVariable("skuId") Long skuId, Model model) throws ExecutionException, InterruptedException {

        System.out.println("准备查询"+skuId);
        SkuItemVo vo = skuInfoService.item(skuId);
        model.addAttribute("item",vo);
        return "item";
    }
}
