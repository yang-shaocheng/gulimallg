package com.yang.gulimall.product.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.yang.gulimall.product.service.CategoryBrandRelationService;
import com.yang.gulimall.product.vo.Catelog2Vo;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yang.common.utils.PageUtils;
import com.yang.common.utils.Query;

import com.yang.gulimall.product.dao.CategoryDao;
import com.yang.gulimall.product.entity.CategoryEntity;
import com.yang.gulimall.product.service.CategoryService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {


    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private RedissonClient redisson;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithtree() {
        //1.查出所有分类
        List<CategoryEntity> entities = baseMapper.selectList(null);
        //2.组装成父子树形结构
        List<CategoryEntity> level1Menus = entities.stream().filter((categoryEntity) -> {
            return categoryEntity.getParentCid() == 0;
        }).map((menu) -> {
            menu.setChildren(getChildren(menu, entities));
            return menu;
        }).sorted((menu1, menu2) -> {
            return (menu1.getSort() == null ? 0 : menu1.getSort()) - (menu2.getSort() == null ? 0 : menu2.getSort());
        }).collect(Collectors.toList());
        return level1Menus;
    }

    @Override
    public void removeMenusByIds(List<Long> asList) {
        //判断是否可删除条件：检查当前删除的菜单是否被别的地方应用或者还有子菜单

        //逻辑删除
        baseMapper.deleteBatchIds(asList);
    }

    //[2,25,225]
    @Override
    public Long[] findCatelogPath(Long catelogId) {
        List<Long> paths = new ArrayList<>();
        List<Long> parentPath = findParentPath(catelogId, paths);
        Collections.reverse(parentPath);
        return parentPath.toArray(new Long[parentPath.size()]);
    }

    //级联更新所有的数据
//    @Caching(evict = {
//            @CacheEvict(value = "category",key = "'getLevel1Categorys'"),
//            @CacheEvict(value = "category",key = "'getCatalogJson'")
//    })  同时删除2个相关的缓存 方法1

    @CacheEvict(value = "category",allEntries = true) //失效模式  同时删除2个相关的缓存 方法2  存储同一类型的数据，都可以指定成同一个分区，批量删除
    @Override
    @Transactional
//    @CachePut 双写模式使用它，但是要有方法要有返回值
    public void updateCascade(CategoryEntity category) {
        //更新自己
        this.updateById(category);
        categoryBrandRelationService.updateCategory(category.getCatId(), category.getName());
        //双写模式，改的同时修改缓存中的数据，对于不怎么经常更新的场景，加读写锁，基本与不加锁没区别
        //失效模式，改完了删掉缓存中的数据,等待下次主动查询进行更新
    }
    //1.每一个需要缓存的数据，我们都来指定要放到哪个名字的缓存分区
    //2、@Cacheable({"category"})  //代表当前方法的结果需要缓存，如果缓存中有，方法不用调用，如果缓存没有，会调用方法，最后将方法的结果放入缓存
    //3、默认行为：1.如果缓存中有，方法不用调用 2.key默认自动生成：缓存的名字::SimpleKey[]  ---category::SimpleKey[]
    //           3.缓存的value值。默认使用jdk序列化机制，将序列化后的数据存入redis 4.过期时间-1，永不过期
    //4、自定义操作：1、指定key：key属性指定，接收一个spEL（这个只能改SimpleKey[]的值，前缀的值在配置文件中）2、指定过期时间:配置文件中设置
    //              3、value以json格式保存:cacheAutoConfiguration导入RedisAutoConfiguration注入RedisCacheManager按照配置yaml初始化所有的缓存（有配置用配置，没有用默认）
    //                    所以想改缓存配置，只需要给容器中放一个RedisAutoConfiguration配置类，就会应用到当前RedisCacheManager管理的所有缓存分区中
    //5、spring-cache的不足：1.读模式：缓存穿透：查询一个null数据 解决：存储空数据，缓存击穿：大量并发进来同时查询一个正好过期的数据，解决：加锁？默认是无加锁的。sync = true(加锁，解决)缓存雪崩：大量的key同时过期，解决，随机时间
    //                     2.写模式：（缓存与数据库一直） 1、读写加锁（读多写少场景 ）。2、引入cannal感知mysql更新
//                  总结：常规数据（读多写少，即时性，一致性要求不高的数据），完全可以使用spring-cache ，特殊数据：需要特殊设计
    @Override
    @Cacheable(value = {"category"},key = "#root.method.name",sync = true)
    public List<CategoryEntity> getLevel1Categorys() {
        System.out.println("getLevel1Category");
        long l = System.currentTimeMillis();
        List<CategoryEntity> entities = baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("parent_cid", 0));

        return entities;
    }
    @Cacheable(value = "category",key = "#root.methodName")
    @Override
    public Map<String, List<Catelog2Vo>> getCatalogJson(){
        System.out.println("查询了数据库....");
        //优化分类获取，将多次数据库查询变为一次
        List<CategoryEntity> categoryEntities = baseMapper.selectList(null);
        //1.拿到所有一级分类
        List<CategoryEntity> level1Categorys = getParent_cid(categoryEntities, 0L);
        //2.查出所有一级分类的子分类, map中 K是一级分类的ID V是2级分类这个VO的数组
        Map<String, List<Catelog2Vo>> parent_cid = level1Categorys.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
            //1.每一个的一级分类，查这个一级分类的所有2级分类
            List<CategoryEntity> category2Entities = getParent_cid(categoryEntities, v.getCatId());
            //2.封装上面的结果 List<CategoryEntity>  -- >  List<Catelog2Vo>
            List<Catelog2Vo> catelog2Vos = null;
            if (category2Entities != null) {
                catelog2Vos = category2Entities.stream().map(l2 -> {
                    Catelog2Vo catelog2Vo = new Catelog2Vo(v.getCatId().toString(), null, l2.getCatId().toString(), l2.getName());
                    //3.找当前2级分类的三级分类
                    List<CategoryEntity> category3Entities = getParent_cid(categoryEntities, l2.getCatId());
                    if (category3Entities != null) {
                        //封装指定格式
                        List<Catelog2Vo.Catalog3Vo> collect = category3Entities.stream().map(l3 -> {
                            Catelog2Vo.Catalog3Vo catelog3Vo = new Catelog2Vo.Catalog3Vo(l2.getCatId().toString(), l3.getCatId().toString(), l3.getName());
                            return catelog3Vo;
                        }).collect(Collectors.toList());
                        catelog2Vo.setCatalog3List(collect);
                    }
                    return catelog2Vo;
                }).collect(Collectors.toList());
            }
            return catelog2Vos;
        }));
        //3、查到的数据放入缓存、将map对象要转为json放在缓存中
        String s = JSON.toJSONString(parent_cid);
        redisTemplate.opsForValue().set("catalogJSON", s, 1, TimeUnit.DAYS);
        return parent_cid;
    }
    //TODO:产生堆外内存溢出需要解决

    public Map<String, List<Catelog2Vo>> getCatalogJson2() {
        //给缓存中放json字符串，拿出的json字符串，还用逆转为能用的map：这叫序列化与反序列化
        /**
         * 1、空结果缓存：解决缓存缓存
         * 2、设置过期时间（加随机值），解决缓存雪崩
         * 3、加锁：解决缓存击穿
         */
        //1、加入缓存逻辑，缓存中存的数据是json字符串 json跨语言，跨平台兼容
        String catalogJSON = redisTemplate.opsForValue().get("catalogJSON");
        if (StringUtils.isEmpty(catalogJSON)) {
            //2、缓存中没有,查询数据库
            //保存数据库查询完成以后，将数据放在redis中，这是一个原子操作
            System.out.println("缓存不命中。。。。将要查询数据库。。。");
            Map<String, List<Catelog2Vo>> catalogJsonFromDb = getCatalogJsonFromDbWithRedisLock();
            return catalogJsonFromDb;
        }
        System.out.println("缓存命中...直接返回。。。。");
        //把catalogJSON逆转为指定的对象
        Map<String, List<Catelog2Vo>> result = JSON.parseObject(catalogJSON, new TypeReference<Map<String, List<Catelog2Vo>>>() {
        });
        return result;
    }

    /**
     * 缓存里面的数据如何和数据库保持一致
     * 缓存数据一致性问
     * 1.双写模式
     * 2.失效模式
     * @return
     */
    public Map<String, List<Catelog2Vo>> getCatalogJsonFromDbWithRedissonLock() {
        //1、占分布式锁，去redis占坑 锁的名字决定相关业务占的锁是同一把，锁的粒度越细，程序越快(因为不会影响不相关的业务)
        //锁的力度：具体缓存的某个数据，11-号商品：product-11-lock
        RLock lock = redisson.getLock("catalogJson-lock");
        //加锁
        lock.lock();
        System.out.println("获取分布式锁成功");
            Map<String, List<Catelog2Vo>> dataFromDb;
            try {
                dataFromDb = getDataFromDb();
            }finally {
               lock.unlock();
            }
            return dataFromDb;
    }
    public Map<String, List<Catelog2Vo>> getCatalogJsonFromDbWithRedisLock() {

        //如果value值是固定，有可能因为业务时间过长超过设置的过期，导致解锁操作误删成别人的锁，引起失控
        String uuid = UUID.randomUUID().toString();
        //1、占分布式锁，去redis占坑 ,并且给一个过期时间，防止内存溢出 30秒后自动删
        Boolean lock = redisTemplate.opsForValue().setIfAbsent("lock", uuid,300,TimeUnit.SECONDS);
        if (lock){
            System.out.println("获取分布式锁成功");
            //加锁成功，执行业务
//            //给一个过期时间，防止内存溢出 30秒后自动删 这种也不稳定，要设置锁和删除时间是一个原子操作
//            redisTemplate.expire("lock",30,TimeUnit.SECONDS);
            Map<String, List<Catelog2Vo>> dataFromDb;
            try {
                dataFromDb = getDataFromDb();
            }finally {
                String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
              Long lock1 = redisTemplate.execute(new DefaultRedisScript<Long>(script, Long.class), Arrays.asList("lock"), uuid);
            }
            //解锁
//            redisTemplate.delete("lock");
            //这个操作要跟redis交互，消耗网络很费事，有可能拿到值虽然是自己的，但是回来过程中锁已经超时自己删除了且其他的进程进来了设置了新值，
            // 然后秒删就会影响其他进程，所以要求获取值对比+对比成功删除也是一个原子操作 lua脚本解锁
//            String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
//            Integer lock1 = redisTemplate.execute(new DefaultRedisScript<Integer>(script, Integer.class), Arrays.asList("lock"), uuid);
//            String lockValue = redisTemplate.opsForValue().get("lock");
//            //判断现在这个锁是不是我自己的锁
//            if (uuid.equals(lockValue)){
//                //删除我自己的锁
//                redisTemplate.delete("lock");
//            }
            return dataFromDb;
        }else {
            //加锁失败..重试
            //休眠200ms
            System.out.println("获取分布式锁失败。。等待重试");
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            return  getCatalogJsonFromDbWithRedisLock();//自旋的方式
        }


    }

    private Map<String, List<Catelog2Vo>> getDataFromDb() {
        String catalogJSON = redisTemplate.opsForValue().get("catalogJSON");
        if (!StringUtils.isEmpty(catalogJSON)) {
            //如果缓存不为空，说明上次抢到锁的那个线程已经往里面存了数据了，直接返回即可
            Map<String, List<Catelog2Vo>> result = JSON.parseObject(catalogJSON, new TypeReference<Map<String, List<Catelog2Vo>>>() {
            });
            return result;
        }
        System.out.println("查询了数据库....");
        //优化分类获取，将多次数据库查询变为一次
        List<CategoryEntity> categoryEntities = baseMapper.selectList(null);

        //1.拿到所有一级分类
        List<CategoryEntity> level1Categorys = getParent_cid(categoryEntities, 0L);
        //2.查出所有一级分类的子分类, map中 K是一级分类的ID V是2级分类这个VO的数组
        Map<String, List<Catelog2Vo>> parent_cid = level1Categorys.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
            //1.每一个的一级分类，查这个一级分类的所有2级分类
            List<CategoryEntity> category2Entities = getParent_cid(categoryEntities, v.getCatId());
            //2.封装上面的结果 List<CategoryEntity>  -- >  List<Catelog2Vo>
            List<Catelog2Vo> catelog2Vos = null;
            if (category2Entities != null) {
                catelog2Vos = category2Entities.stream().map(l2 -> {
                    Catelog2Vo catelog2Vo = new Catelog2Vo(v.getCatId().toString(), null, l2.getCatId().toString(), l2.getName());
                    //3.找当前2级分类的三级分类
                    List<CategoryEntity> category3Entities = getParent_cid(categoryEntities, l2.getCatId());
                    if (category3Entities != null) {
                        //封装指定格式
                        List<Catelog2Vo.Catalog3Vo> collect = category3Entities.stream().map(l3 -> {
                            Catelog2Vo.Catalog3Vo catelog3Vo = new Catelog2Vo.Catalog3Vo(l2.getCatId().toString(), l3.getCatId().toString(), l3.getName());
                            return catelog3Vo;
                        }).collect(Collectors.toList());
                        catelog2Vo.setCatalog3List(collect);
                    }
                    return catelog2Vo;
                }).collect(Collectors.toList());
            }
            return catelog2Vos;
        }));
        //3、查到的数据放入缓存、将map对象要转为json放在缓存中
        String s = JSON.toJSONString(parent_cid);
        redisTemplate.opsForValue().set("catalogJSON", s, 1, TimeUnit.DAYS);
        return parent_cid;
    }

    //从数据库查询并封装分类数据
    public Map<String, List<Catelog2Vo>> getCatalogJsonFromDbWithLocalLock() {
        //只要是同一把锁，就能锁住需要这个锁的所有线程
        //1.synchronized (this)：这个this是categoryService对象，他在springboot所有组件在IOC容器中都是单例的，因此他是同一把锁
        //TODO:但是这个也只是本地锁，只能锁这台服务器承载的线程，分布式场景下其他服务器的线程管不到，但是我感觉其实也缓解了很多了，只有服务器个数的线程,分布式场景下，想要锁住所有要使用分布式锁
        synchronized (this) {
            //这里解锁了后，其他的线程进来还是要查数据库，和需要的想法就不匹配，因此这里进来还是要再去缓存中看是否有数据，如果没有才需要继续查数据库
            return getDataFromDb();
        }
    }

    //在所有分类总和的集合里，找到parent_cid等于传入值的集合
    private List<CategoryEntity> getParent_cid(List<CategoryEntity> categoryEntities, Long parent_cid) {
        List<CategoryEntity> collect = categoryEntities.stream().filter(item -> item.getParentCid() == parent_cid).collect(Collectors.toList());
        return collect;
    }

    //写一个递归方法，收集当前节点catID,并查找父节点的ID
    public List<Long> findParentPath(Long catelogId, List<Long> path) {
        //1.收集当前id
        path.add(catelogId);
        CategoryEntity id = this.getById(catelogId);
        if (id.getParentCid() != 0) {
            findParentPath(id.getParentCid(), path);
        }
        return path;
    }

    //在所有entities中查询某一个categoryEntity的子菜单,递归查找
    private List<CategoryEntity> getChildren(CategoryEntity root, List<CategoryEntity> all) {
        List<CategoryEntity> children = all.stream().filter((categoryEntity -> {
            return categoryEntity.getParentCid() == root.getCatId();
        })).map((categoryEntity) -> {
            //1,找到子菜单
            categoryEntity.setChildren(getChildren(categoryEntity, all));
            return categoryEntity;
            //2.菜单的排序
        }).sorted((menu1, menu2) -> {
            return (menu1.getSort() == null ? 0 : menu1.getSort()) - (menu2.getSort() == null ? 0 : menu2.getSort());
        }).collect(Collectors.toList());
        return children;
    }

}