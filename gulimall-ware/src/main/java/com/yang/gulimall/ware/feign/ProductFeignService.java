package com.yang.gulimall.ware.feign;

import com.yang.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * ClassName: ProductFeignService
 * Package: com.yang.gulimall.ware.feign
 * description:
 */
@FeignClient("gulimall-product")
public interface ProductFeignService {
    //远程查询产品SKU
    @RequestMapping("/product/skuinfo/info/{skuId}")
    //@RequiresPermissions("product:skuinfo:info")
    public R info(@PathVariable("skuId") Long skuId);
}
