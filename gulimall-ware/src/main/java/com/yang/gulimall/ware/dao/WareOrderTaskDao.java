package com.yang.gulimall.ware.dao;

import com.yang.gulimall.ware.entity.WareOrderTaskEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存工作单
 * 
 * @author yangshaocheng
 * @email 515085554@qq.com
 * @date 2024-01-25 16:57:31
 */
@Mapper
public interface WareOrderTaskDao extends BaseMapper<WareOrderTaskEntity> {
	
}
