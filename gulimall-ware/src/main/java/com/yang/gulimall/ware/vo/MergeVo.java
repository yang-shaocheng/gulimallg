package com.yang.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * ClassName: MergeVo
 * Package: com.yang.gulimall.ware
 * description:
 */
@Data
public class MergeVo {
    private  Long purchaseId;
    private List<Long>  items;
 }
