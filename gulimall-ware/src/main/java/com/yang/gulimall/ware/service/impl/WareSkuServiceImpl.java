package com.yang.gulimall.ware.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.rabbitmq.client.Channel;
import com.yang.common.to.OrderTo;
import com.yang.common.to.mq.StockDetailTo;
import com.yang.common.to.mq.StockLockedTo;
import com.yang.common.utils.R;
import com.yang.gulimall.ware.entity.WareOrderTaskDetailEntity;
import com.yang.gulimall.ware.entity.WareOrderTaskEntity;
import com.yang.gulimall.ware.exception.NoStockException;
import com.yang.gulimall.ware.feign.OrderFeignService;
import com.yang.gulimall.ware.feign.ProductFeignService;
import com.yang.gulimall.ware.service.WareOrderTaskDetailService;
import com.yang.gulimall.ware.service.WareOrderTaskService;
import com.yang.gulimall.ware.vo.*;
import lombok.Data;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yang.common.utils.PageUtils;
import com.yang.common.utils.Query;

import com.yang.gulimall.ware.dao.WareSkuDao;
import com.yang.gulimall.ware.entity.WareSkuEntity;
import com.yang.gulimall.ware.service.WareSkuService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;


@Service("wareSkuService")
@RabbitListener(queues = "stock.release.stock.queue")
public class WareSkuServiceImpl extends ServiceImpl<WareSkuDao, WareSkuEntity> implements WareSkuService {
    @Autowired
    private  WareSkuDao wareSkuDao;
    @Autowired
    private ProductFeignService productFeignService;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private WareOrderTaskDetailService orderTaskDetailService;
    @Autowired
    private WareOrderTaskService orderTaskService;
    @Autowired
    private OrderFeignService orderFeignService;

    private  void  unLockStock(Long skuId,Long wareId,Integer num,Long taskDetailId){
        //库存解锁
        wareSkuDao.unlockStock(skuId,wareId,num);
        //更新库存工作单的状态
        WareOrderTaskDetailEntity entity = new WareOrderTaskDetailEntity();
        entity.setId(taskDetailId);
        entity.setLockStatus(2);//变为已解锁
        orderTaskDetailService.updateById(entity);

    }
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<WareSkuEntity> wrapper = new QueryWrapper<>();
        String skuId = (String) params.get("skuId");
        if(!StringUtils.isEmpty(skuId)){
            wrapper.and(w->{
                w.eq("sku_id",skuId);
            });
        }
        String wareId = (String) params.get("wareId");
        if(!StringUtils.isEmpty(wareId)){
            wrapper.eq("ware_id",wareId);
        };

        IPage<WareSkuEntity> page = this.page(
                new Query<WareSkuEntity>().getPage(params),
                wrapper
        );

        return new PageUtils(page);
    }

    @Override
    public void addStock(Long skuId, Long wareId, Integer skuNum) {
        //1.判断如果这个库存记录一开始一个都没有
        List<WareSkuEntity> wareSkuEntities = wareSkuDao.selectList(new QueryWrapper<WareSkuEntity>().eq("sku_id", skuId).eq("ware_id", wareId));
        if (wareSkuEntities == null || wareSkuEntities.size() == 0){
            WareSkuEntity entity = new WareSkuEntity();
            entity.setSkuId(skuId);
            entity.setStock(skuNum);
            entity.setWareId(wareId);
            entity.setStockLocked(0);
            //TODO :自己catch异常，如果有问题，不用回滚
            //远程查询SKU的名字
            try {
                R info = productFeignService.info(skuId);
                Map<String,Object> skuInfo = (Map<String, Object>) info.get("skuInfo");
                if (info.getCode() == 0){
                    entity.setSkuName((String) skuInfo.get("skuName"));
                }
            }catch (Exception e){

            }
            wareSkuDao.insert(entity);
        }else {
            wareSkuDao.addStock(skuId,wareId,skuNum);
        }

    }

    @Override
    public List<SkuHasStockVo> getSkusHasStock(List<Long> skuIds) {
        List<SkuHasStockVo> collect = skuIds.stream().map(skuId -> {
            SkuHasStockVo vo = new SkuHasStockVo();
            //查询当前sku的总库存量,某一个商品有可能在多个仓库都有库存
            Long count = baseMapper.getSkuStock(skuId);
            vo.setSkuId(skuId);
            vo.setHasStock(count == null?false:count > 0);
            return vo;
        }).collect(Collectors.toList());
        return collect;
    }


    /**
     *
     * 为某个订单锁定库存
     * 默认运行时异常都会回滚,rollbackFor = NoStockException.class可以不用
     *
     * 库存解锁场景
     * 1）、下订单成功，订单过期没有支付
     * 2）、订单被手动取消了
     * 3)、下面其他服务失败了，如果扣减积分
     *
     */
    @Transactional(rollbackFor = NoStockException.class)
    @Override
    public Boolean orderLockStock(WareSkuLockVo vo) {
        //0.保存库存工作单，延时队列追述的时候用
        WareOrderTaskEntity taskEntity = new WareOrderTaskEntity();
        taskEntity.setOrderSn(vo.getOrderSn());
        orderTaskService.save(taskEntity);


        //1.找到每个商品在哪个仓库都有库存
        List<OrderItemVo> locks = vo.getLocks();
        List<skuWareHasStock> collect = locks.stream().map(item -> {
            skuWareHasStock stock = new skuWareHasStock();
            Long skuId = item.getSkuId();
            stock.setSkuId(skuId);
            stock.setNum(item.getCount());
            //查询这个商品在哪里有库存
            List<Long> wareIds = wareSkuDao.listWareIdHasSkuStock(skuId);
            stock.setWareId(wareIds);
            return stock;
        }).collect(Collectors.toList());
        //2.锁定库存
        for (skuWareHasStock hasStock : collect) {
            Boolean skuStocked = false;
            Long skuId = hasStock.getSkuId();
            List<Long> wareIds = hasStock.getWareId();
            if (wareIds == null||wareIds.size() == 0){
                //没有任何仓库有这个商品的库存
                throw  new NoStockException(skuId);
            }
            //1.如果每一个商品都锁定成功，将当前商品锁定了几件的工作单记录发给MQ
            //2.如果锁定失败，本地事务会将前面保存的工作单信息回滚，不过发送出去的消息也不影响，因为它要解锁记录要去数据库查，会发现数据库的记录已经被本地事务删除，所以就不用解锁
            for (Long wareId : wareIds) {
                //成功返回1，失败0
              Long count =  wareSkuDao.lockSkuStock(skuId,wareId,hasStock.getNum());
              if (count == 1){
                    //这个仓库锁成功了，挑出循环
                   skuStocked = true;
                   //给MQ发送消息前保存每个商品锁定成功的详细信息
                  WareOrderTaskDetailEntity entity = new WareOrderTaskDetailEntity(null, skuId, "", hasStock.getNum(), taskEntity.getId(), wareId, 1);
                  orderTaskDetailService.save(entity);
                  //TODO:告诉MQ库存锁定成功
                  StockLockedTo lockedTo = new StockLockedTo();
                    lockedTo.setId(taskEntity.getId());
                  StockDetailTo stockDetailTo = new StockDetailTo();
                  BeanUtils.copyProperties(entity,stockDetailTo);
                  //不能只发ID，不然回滚的时候不知道要回多少个，因为 锁成功详情表的里面的信息已经被本地事务删除
                    lockedTo.setDetailTo(stockDetailTo);
                  rabbitTemplate.convertAndSend("stock-event-exchange","stock.locked",lockedTo);
                  break;
              }else {
                  //当前仓库锁失败，重试下一个仓库
              }
            }
            if (skuStocked == false){
                //当前商品所有仓库都没有锁住
                throw  new NoStockException(skuId);
            }
        }
        //3.能走到这，说明所有商品都锁定成功

        return true;
    }

    @Override
    public void unlockStock(StockLockedTo to) {

        StockDetailTo detail = to.getDetailTo();
        Long skuId = detail.getSkuId();//当前这个延迟消息的详细任务单的skuId
        Long detailId = detail.getId();//当前这个延迟消息的详细任务单的主键ID
        //解锁
        //1.查询数据库关于这个订单的锁定库存信息
        //有：说明本身锁库存这个操作是没问题,下面又2中情况：1.因为其他业务调用失败如积分扣减导致 订单业务回滚但是库存业务不回滚，需要监听延迟消息来解锁2.业务执行成功了有这个订单，此时需要看订单状态，（1）如果订单已取消，则解锁，（2）没取消则不解锁
        //没有：说明锁库存这个操作失败了，如某一个商品库存不够，本地事务已经把库存解锁了，就不需要再监听消息这个再来解锁了
        WareOrderTaskDetailEntity byId = orderTaskDetailService.getById(detailId);
        if (byId!=null){
            //解锁
            Long id = to.getId();//库存工作单的ID
            WareOrderTaskEntity taskEntity = orderTaskService.getById(id);
            String orderSn = taskEntity.getOrderSn();//通过库存工作单的ID查到对应的ordsn再去查这个订单是否已经取消
            R orderStatus = orderFeignService.getOrderStatus(orderSn);
            if (orderStatus.getCode()==0){
                //订单数据返回成功
                OrderVo data = orderStatus.getData(new TypeReference<OrderVo>() {});
                if (data == null || data.getStatus() == 4){
                    //订单不存在（==null,说明在下单任务没有执行完，如扣减积分失败）或订单已经被取消了（status==4）,才能解锁库存
                    if (byId.getLockStatus()==1){
                        //当前库存工作单详情状态1已锁定，但是未解锁才执行解锁
                    unLockStock(detail.getSkuId(),detail.getWareId(),detail.getSkuNum(),detailId);
                    }
                }
            }else {
                //由于远程调用等原因执行失败，将消息退回给queue重新执行
                throw  new RuntimeException("远程服务失败");
            }
        }
        else {
            //无需解锁

        }
    }
    @Transactional
    //防止订单服务卡顿导致订单关闭的消息比解锁库存的消息后处理，导致库存解锁消息永远无法执行，他先查到订单是新建状态，什么都不做，后面订单状态改了后，解锁库存消息已经没了
    @Override
    public void unlockStock(OrderTo to) {
        String orderSn = to.getOrderSn();
        //查一下最新库存的状态，防止重复解锁库存
        WareOrderTaskEntity task = orderTaskService.getOrderTaskByOrderSn(orderSn);
        //根据工作单 task_id找到所有管理的详情单里面状态不是已解锁的
        List<WareOrderTaskDetailEntity> list = orderTaskDetailService.list(new QueryWrapper<WareOrderTaskDetailEntity>().eq("task_id", task.getId()).eq("lock_status",1));
        for (WareOrderTaskDetailEntity entity : list) {
            unLockStock(entity.getSkuId(),entity.getWareId(),entity.getSkuNum(),entity.getId());
        }
    }

    @Data
    class  skuWareHasStock{
        private Long skuId;
        private Integer num;
        private List<Long> wareId;
    }
}