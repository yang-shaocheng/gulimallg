package com.yang.gulimall.search;


import com.alibaba.fastjson.JSON;
import com.yang.gulimall.search.config.GulimallElasticsearchConfig;
import lombok.Data;
import lombok.ToString;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.Avg;
import org.elasticsearch.search.aggregations.metrics.AvgAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimallSearchApplicationTests {

	@Autowired
	private RestHighLevelClient client;
	@ToString
	@Data
	static class Account {
		private int account_number;
		private int balance;
		private String firstname;
		private String lastname;
		private int age;
		private String gender;
		private String address;
		private String employer;
		private String email;
		private String city;
		private String state;
	}
	@Test
	public void searchData() throws IOException {
	    //1.创建一个检索请求  sourceBuilder封装检索条件
		SearchRequest searchRequest = new SearchRequest();
		//指定索引
		searchRequest.indices("bank");
		//指定DSL 检索条件
		SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
		//1.1 匹配查询条件
		sourceBuilder.query(QueryBuilders.matchQuery("address","mill"));
		//1.2 按照年龄分布聚合
		TermsAggregationBuilder ageAgg = AggregationBuilders.terms("ageAgg").field("age").size(10);
		sourceBuilder.aggregation(ageAgg);
		//1.3 计算平均薪资
		AvgAggregationBuilder balanceAgg = AggregationBuilders.avg("balanceAgg").field("balance");
		sourceBuilder.aggregation(balanceAgg);



		System.out.println("sourceBuilder = " + sourceBuilder.toString());
		searchRequest.source(sourceBuilder);
		//2 执行检索
		SearchResponse search = client.search(searchRequest, GulimallElasticsearchConfig.COMMON_OPTIONS);
		//3.分析结果
		System.out.println( search.toString());
//		Map map = JSON.parseObject(search.toString(), Map.class);麻烦
		//3.1获取所有查到的数据
		SearchHits hits = search.getHits();//外层的大HITS
		SearchHit[] searchHits = hits.getHits();// 真正命中的所有记录
		for (SearchHit searchHit : searchHits) {
			String string = searchHit.getSourceAsString();
			Account account = JSON.parseObject(string, Account.class);
			System.out.println("account = " + account);
		}
		//3.2获取这次检索到的分析信息
		Aggregations aggregations = search.getAggregations();
//		for (Aggregation aggregation : aggregations.asList()) {
//			System.out.println("当前聚合"+aggregation);
//
//		}
		Terms agg1 = aggregations.get("ageAgg");
		for (Terms.Bucket bucket : agg1.getBuckets()) {
			String string = bucket.getKeyAsString();
			System.out.println("年龄 = " + string);
		}
		Avg agg2 = aggregations.get("balanceAgg");
		System.out.println("平均薪资 = " + agg2.getValue());

	}
	//测试存储数据到ES
	@Test
	public void indexData() throws IOException {
		//1.保存数据
		IndexRequest indexRequest = new IndexRequest("users");
		indexRequest.id("1");
		@Data
		class  User{
			private  String userName;
			private  String gender;
			private  Integer age;
		}
		User user = new User();
		user.setUserName("yangshaocheng");
		user.setAge(33);
		user.setGender("m");
		String json = JSON.toJSONString(user);
		indexRequest.source(json, XContentType.JSON);
		//2.执行操作
		IndexResponse index = client.index(indexRequest, GulimallElasticsearchConfig.COMMON_OPTIONS);
		//提取有用的相应数据
		System.out.println("index = " + index);

	}
	@Test
	public void contextLoads() {
		System.out.println("client = " + client);
	}

}
