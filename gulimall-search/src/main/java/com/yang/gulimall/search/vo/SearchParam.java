package com.yang.gulimall.search.vo;

import lombok.Data;

import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * ClassName: SearchParam
 * Package: com.yang.gulimall.search.vo
 * description:
 */
@Data
public class SearchParam {
    private  String keyword;//页面传过来的全文匹配关键字
    private  Long catalog3Id;//三级分类ID
    private  String sort;//排序条件
    private  Integer hasStock ;//是否显示有货
    private  String skuPrice;//价格区间
    private List<Long> brandId;// 选中的品牌,可以多选
    private List<String> attrs;//按照属性进行筛选，也可以多个
    private  Integer pageNum = 1;//页码
    private String _queryString;//原生的所有查询条件


}
