package com.yang.gulimall.search.controller;

import com.yang.gulimall.search.service.MallSearchService;
import com.yang.gulimall.search.vo.SearchParam;
import com.yang.gulimall.search.vo.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * ClassName: SearchController
 * Package: com.yang.gulimall.search.controller
 * description:
 */
@Controller
public class SearchController {
    @Autowired
    MallSearchService searchService;
    @GetMapping("/list.html")
    public  String listPage(SearchParam param, Model model, HttpServletRequest request){
        String queryString = request.getQueryString();
        param.set_queryString(queryString);
        //根据传递来的参数，去es中检索商品
       SearchResult result = searchService.search(param);
       model.addAttribute("result",result);


        return  "list";
    }
}
