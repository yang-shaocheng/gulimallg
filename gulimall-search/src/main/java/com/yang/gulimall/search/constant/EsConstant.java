package com.yang.gulimall.search.constant;

/**
 * ClassName: EsConstant
 * Package: com.yang.gulimall.search.constant
 * description:
 */
public class EsConstant {
    public static  final  String PRODUCT_INDEX = "gulimall_product";//sku数据在ES中的索引
    public static  final  Integer PRODUCT_PAGESIZE = 16;//默认的分页size
}
