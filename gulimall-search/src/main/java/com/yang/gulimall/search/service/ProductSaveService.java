package com.yang.gulimall.search.service;

import com.yang.common.to.es.SkuEsModel;

import java.io.IOException;
import java.util.List;

/**
 * ClassName: ProductSaveService
 * Package: com.yang.gulimall.search.service
 * description:
 */

public interface ProductSaveService {
    boolean productStatusUp(List<SkuEsModel> skuEsModels) throws IOException;
}
