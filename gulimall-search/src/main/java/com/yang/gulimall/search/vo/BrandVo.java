package com.yang.gulimall.search.vo;

import lombok.Data;

/**
 * ClassName: BrandVo
 * Package: com.yang.gulimall.search.vo
 * description:
 */
@Data
public class BrandVo {
    private  Long brandId;
    private  String brandName;
}
