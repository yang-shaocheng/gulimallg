package com.yang.gulimall.search.service;

import com.yang.gulimall.search.vo.SearchParam;
import com.yang.gulimall.search.vo.SearchResult;

/**
 * ClassName: MallSearchService
 * Package: com.yang.gulimall.search.service
 * description:
 */
public interface MallSearchService {
    //根据检索的所有参数返回检索的结果
    SearchResult search(SearchParam param);
}
