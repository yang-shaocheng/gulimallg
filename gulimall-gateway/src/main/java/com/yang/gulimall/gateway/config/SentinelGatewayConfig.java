package com.yang.gulimall.gateway.config;

import com.alibaba.csp.sentinel.adapter.gateway.sc.callback.BlockRequestHandler;
import com.alibaba.csp.sentinel.adapter.gateway.sc.callback.GatewayCallbackManager;
import com.alibaba.fastjson.JSON;
import com.yang.common.exception.BizCodeEnume;
import com.yang.common.utils.R;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * ClassName: SentinelGatewayConfig
 * Package: com.yang.gulimall.gateway.config
 * description:
 */
@Configuration
public class SentinelGatewayConfig {
        public  SentinelGatewayConfig (){
            GatewayCallbackManager.setBlockHandler(new BlockRequestHandler() {
                //网关限流了请求，就会调用这个回调
                @Override
                public Mono<ServerResponse> handleRequest(ServerWebExchange serverWebExchange, Throwable throwable) {
                    R error = R.error(BizCodeEnume.TOO_MANY_REQUEST.getCode(), BizCodeEnume.TOO_MANY_REQUEST.getMsg());
                    String string = JSON.toJSONString(error);
                    Mono<ServerResponse> body = ServerResponse.ok().body(Mono.just(string), String.class);
                    return body;
                }
            });
        }
}
