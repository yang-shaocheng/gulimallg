package com.yang.gulimall.auth.feign;

import com.yang.common.utils.R;
import com.yang.gulimall.auth.vo.UserLoginVo;
import com.yang.gulimall.auth.vo.UserRegistVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * ClassName: MemberFeignService
 * Package: com.yang.gulimall.auth.feign
 * description:
 */
@FeignClient("gulimall-member")
public interface MemberFeignService {

    @PostMapping("/member/member/regist")
    public R regist(@RequestBody UserRegistVo vo);

    @PostMapping("/member/member/login")
    public  R login(@RequestBody UserLoginVo vo);
}
