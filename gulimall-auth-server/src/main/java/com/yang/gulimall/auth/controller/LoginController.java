package com.yang.gulimall.auth.controller;

import com.alibaba.fastjson.TypeReference;
import com.yang.common.constant.AuthServerConstant;
import com.yang.common.exception.BizCodeEnume;
import com.yang.common.utils.R;
import com.yang.common.vo.MemberResponseVo;
import com.yang.gulimall.auth.feign.MemberFeignService;
import com.yang.gulimall.auth.feign.ThirdPartFeignService;
import com.yang.gulimall.auth.vo.UserLoginVo;
import com.yang.gulimall.auth.vo.UserRegistVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * ClassName: LoginController
 * Package: com.yang.gulimall.auth.controller
 * description:
 */
@Slf4j
@Controller
public class LoginController {
    @Autowired
    private ThirdPartFeignService thirdPartFeignService;
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private MemberFeignService memberFeignService;
        @ResponseBody
        @GetMapping("/sms/sendcode")
        public R sendCode(@RequestParam("phone") String phone){
            //1.接口防刷
            String redisCode = redisTemplate.opsForValue().get(AuthServerConstant.SMS_CODE_CACHE_PREFIX + phone);
            if (!StringUtils.isEmpty(redisCode)){
            //得到当时存入验证码时候的时间
            long l = Long.parseLong(redisCode.split("_")[1]);
            if (System.currentTimeMillis() - l < 60000){
                return R.error(BizCodeEnume.VAILD_sms_EXCEPTION.getCode(), BizCodeEnume.VAILD_sms_EXCEPTION.getMsg());
            }
            }

            //2。验证码的再次校验---存到redis  key-phone value--code
            Random random = new Random();
            int i = 1000+random.nextInt(10000);
            String code1 = i+"_"+System.currentTimeMillis();
//            String code =UUID.randomUUID().toString().substring(0, 4)+"_"+System.currentTimeMillis();
            //为了登录时候在规定时间内可以校验验证码，将得到的验证码存入redis,同时要防止同一个phone在60秒内再次发送验证码
            redisTemplate.opsForValue().set(AuthServerConstant.SMS_CODE_CACHE_PREFIX+phone,code1,10, TimeUnit.MINUTES);
            //下的模板要求，发送的时候要拼串
            code1 = "code:"+code1.split("_")[0];
            thirdPartFeignService.sendCode(phone,code1);
            return R.ok();
        }
        @PostMapping("/regist")
    private String regist(@Valid UserRegistVo vo, BindingResult result, RedirectAttributes redirectAttributes){
        if (result.hasErrors()){
            Map<String, String> errors = result.getFieldErrors().stream().collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
//            model.addAttribute("errors",errors);
            redirectAttributes.addFlashAttribute("errors",errors);
            //如果校验出错，转发到注册页

            return "redirect:http://auth.gulimall.com/reg.html";
        }
        //没有错误，真正开始注册，调用远程服务
        //1.校验验证码
            String code = vo.getCode();
            String s = redisTemplate.opsForValue().get(AuthServerConstant.SMS_CODE_CACHE_PREFIX + vo.getPhone());
            if (!StringUtils.isEmpty(s)){
            if (code.equals(s.split("_")[0])){
                //校验成功就删除验证码
                redisTemplate.delete(AuthServerConstant.SMS_CODE_CACHE_PREFIX + vo.getPhone());
                //验证码通过
                R r = memberFeignService.regist(vo);
                if (r.getCode() == 0){
                    //成功
                    return "redirect:http://auth.gulimall.com/login.html";
                }else {
                    Map<String, String> errors = new HashMap<>();
                    errors.put("msg", String.valueOf(r.get("msg")));
                    redirectAttributes.addFlashAttribute("errors",errors);
                    return "redirect:http://auth.gulimall.com/reg.html";
                }
            }else {
                //验证码校验错误，验证码错误
                Map<String, String> errors = new HashMap<>();
                errors.put("code","验证码错误");
                redirectAttributes.addFlashAttribute("errors",errors);
                return "redirect:http://auth.gulimall.com/reg.html";
            }
            }else {
                //缓存中没有验证码，验证吗过期
                Map<String, String> errors = new HashMap<>();
                errors.put("code","验证码过期");
                redirectAttributes.addFlashAttribute("errors",errors);
                return "redirect:http://auth.gulimall.com/reg.html";
            }

    }
    @GetMapping("/login.html")
    public String loginPage(HttpSession session){
        Object attribute = session.getAttribute(AuthServerConstant.LOGIN_USER);
        if (attribute == null){
            //没登录，停在登录页
            return "login";
        }else {
            //登录过的,跳转首页
            return "redirect:http://gulimall.com";
        }
    }
     @PostMapping("/login")
    public String login(UserLoginVo vo , RedirectAttributes RedirectAttributes, HttpSession session){
          //远程登录
         R login = memberFeignService.login(vo);
         if (login.getCode() == 0){
             MemberResponseVo data = login.getData(new TypeReference<MemberResponseVo>() {
             });
             session.setAttribute(AuthServerConstant.LOGIN_USER,data);//cookie是需要第一次需要渲染的时候才发给浏览器的
             log.info("登录成功：，用户{}",data.toString());
             return "redirect:http://gulimall.com";
         }else {
             Map<String, String> errors = new HashMap<>();
             errors.put("msg", String.valueOf(login.get("msg")));
             RedirectAttributes.addFlashAttribute("errors",errors);
             return "redirect:http://auth.gulimall.com/login.html";
         }
    }

}
