package com.yang.gulimall.auth.vo;

import lombok.Data;

/**
 * ClassName: UserLoginVo
 * Package: com.yang.gulimall.auth.vo
 * description:
 */
@Data
public class UserLoginVo {
    private  String loginacct;
    private  String password;
}
