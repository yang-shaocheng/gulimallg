package com.yang.gulimall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
//本次一个微服务一个命名空间，用分组区分环境dev,test,proc
//PS；配置中心的优先级高于本地，如果配置项相同
//配置中心操作;1.引入依赖2.编写bootstrap，包含（1.nacos config 信息，2.服务名）3.配置中心图形化添加配置文件（参考springcloud的配置方法）4.业务方法添加@RefreshScope
@SpringBootApplication
@EnableDiscoveryClient
public class GulimallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallCouponApplication.class, args);
    }

}
