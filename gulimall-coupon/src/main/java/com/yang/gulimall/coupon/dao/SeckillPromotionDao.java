package com.yang.gulimall.coupon.dao;

import com.yang.gulimall.coupon.entity.SeckillPromotionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动
 * 
 * @author yangshaocheng
 * @email 515085554@qq.com
 * @date 2024-01-25 15:45:58
 */
@Mapper
public interface SeckillPromotionDao extends BaseMapper<SeckillPromotionEntity> {
	
}
